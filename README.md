# All you need is Docker :) 

## Soft in this docker build

- NGINX
- PHP7 as PHP-FPM
- MariaDB
- Memcached
- Composer
- Nodejs 8
- yarn

### Installation docker env (just easy 3 steps)
1. Create a .env from the .env.dist file and set all required variables.
```bash
$ cp .env.dist .env
```

2. Build/run containers
```bash
$ docker-compose build
$ docker-compose up -d
```

2.1 For Build/run containers with galera cluster
```bash
$ docker-compose -f docker-compose-galera.yml build
$ docker-compose -f docker-compose-galera.yml up -d
```
    or
```bash
$ docker-compose -f docker-compose-galera.yml build
$ docker-compose -f docker-compose-galera.yml up -d --build
```

3. Update your system host file (add r-ulybka.local)
```bash
# unix only (on Windows, edit C:\Windows\System32\drivers\etc\hosts)
$ sudo echo "${APP_IP_ADDRESS} r-ulybka.local" >> /etc/hosts
```

### Composer command
```bash
$ docker-compose exec php bash
$ composer install|update
```

### Nodejs command
```bash
$ docker-compose run nodejs yarn
$ docker-compose run nodejs npm
$ docker-compose run nodejs bash

#Set Up for R-ULYBKA.RU
$ docker-compose run --workdir /var/www/r-ulybka.ru  nodejs yarn
$ docker-compose run --workdir /var/www/r-ulybka.ru  nodejs yarn build
# run bower i
$ docker-compose run --workdir /var/www/r-ulybka.ru  nodejs node_modules/.bin/bower i --allow-root
# run gulp build
$ docker-compose run --workdir /var/www/r-ulybka.ru  nodejs gulp build
```
 
### Useful comands

```bash
# for stop all conteiners
$ docker stop $(docker ps -a -q)
 
# delete all images
$ docker rmi $(docker images -q)
$ docker rmi $(docker images -q) --force
 
# for remove all conteiners
$ docker rm $(docker ps -a -q)
```  

### Bitrix Memcached config

```php
// settings_extra.php
...
'cache' => array(
    'value' => array (
        'type' => 'memcache',
        'memcache' => array(
            'host' => 'memcached',
            'port' => '11211',
            'sid' => $_SERVER["DOCUMENT_ROOT"]."#01",
        ),
    ),
    'readonly' => false,
)
...
```

### Bitrix Mysql config

```php
// settings.php
...
'className' => '\\Bitrix\\Main\\DB\\MysqliConnection',
'host' => 'db',
'database' => '${MYSQL_DATABASE}',
'login' => '${MYSQL_USER}',
'password' => '${MYSQL_PASSWORD}'
...
```